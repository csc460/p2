#include <radio.h>

int const volatile BYTES_TO_SEND = 10;
int const volatile HIGH_BIT = 1;
int const volatile LOW_BIT = 0;
int const volatile JOYSTICK_MID = 557;
//A = 65, B = 66, C = 67, D = 68
int const volatile VALUE_SENT = 65;
int const volatile DONT_DRIVE = 5000;
int const RADIO_VCC = 47; 
int const STOP_COMMANDS_MAX = 10;
int const NUM_ROOMBAS = 4;

// Radio Address 
// int const RADIO_ADDRESS_LENGTH = 5;
uint8_t station_addr[5] = {0xDB, 0xDB, 0xDB, 0xDB, 0xDB};
volatile uint8_t rxflag = 0;

// Pin Mapping
int const xValPin[4] = {0,1,2,3};  // analog pin used to connect the the X coordinate for the joystick
int const yValPin[4] = {4,5,6,7};  // analog pin used to connect the the Y coordinate for the joystick
int const joyStickInPin[4] = {8,9,10,11};  // analog pin used to connect the potentiometer

// Roomba Initialization
int const DRIVE_COMMAND = 137;
int const START_COMMAND = 128;
int const CONTROL_COMMAND = 130;
int const DRIVE_NUM_BYTES = 4;
uint16_t const STRAIGHT = 32768;
int const HIGH_BYTE = 0xFF00;
int const LOW_BYTE = 0x00FF;
int const volatile ROOMBA_X_MAX = 2000;
int const volatile ROOMBA_Y_MAX = 440;
int const ACCELERATION_RATE = 20;

// Non-constants
int volatile timer = 0;
boolean volatile sendByte = false;
boolean volatile enableSend = false;
int volatile bitsSent = 0;
int volatile spotValue = 128;
boolean volatile pin12 = false;
radiopacket_t packet;
pf_command_t command_data;
pf_ir_command_t ircommand_data;
int stopCommandsSent = 0;
int acceleration = 0;


void setup() {
  Serial.begin(9600);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(10, INPUT);
  pinMode(joyStickInPin, INPUT);
  pinMode(RADIO_VCC, OUTPUT);
  
  // Reset the radio
  digitalWrite(RADIO_VCC, LOW_BIT);
  delay(100);
  digitalWrite(RADIO_VCC, HIGH_BIT);
  delay(100);
  
  // Initialize Radio
  Radio_Init();
  
  // configure the receive settings for radio pipe 0
  Radio_Configure_Rx(RADIO_PIPE_0, station_addr, ENABLE);
  // configure radio transciever settings.
  Radio_Configure(RADIO_2MBPS, RADIO_HIGHEST_POWER);
  
  // Loop through roombas and initialize the control command
  for(int i = 0; i < NUM_ROOMBAS; i++)
  {
    // The address to which the next transmission is to be sent
    Radio_Set_Tx_Addr(ROOMBA_ADDRESSES[i]);
    
    // START Command
    memcpy(command_data.sender_address, ROOMBA_ADDRESSES[i], RADIO_ADDRESS_LENGTH);
    command_data.command = START_COMMAND;
    command_data.num_arg_bytes = 0;
    
    packet.type = COMMAND;
    packet.payload.command = command_data;
    
    Radio_Transmit(&packet, RADIO_WAIT_FOR_TX);
    
    // CONTROL Command
    // memcpy(command_data.sender_address, ROOMBA_ADDRESSES[COP1], RADIO_ADDRESS_LENGTH);
    command_data.command = CONTROL_COMMAND;
    command_data.num_arg_bytes = 0;
    
    //packet.type = COMMAND;
    packet.payload.command = command_data;
    
    Radio_Transmit(&packet, RADIO_WAIT_FOR_TX);
  }
}
 
void radio_rxhandler(uint8_t pipe_number)
{
        Serial.print("rxflag=1\n");
	rxflag = 1;
}

int joystick_x_mapping(int val, int range)
{
  if (abs(val - JOYSTICK_MID) > 100)
  {
    return map(val, 0, 1023, -range, range);     // scale it to use it with the servo (value between 0 and 180)
    // myservo.write(val);                  // sets the servo position according to the scaled value
  }
  return DONT_DRIVE;
}

int joystick_y_mapping(int val, int range)
{
  if (abs(val - JOYSTICK_MID) > 100)
  {
    return map(val, 0, 1023, range, -range);     // scale it to use it with the servo (value between 0 and 180)
    // myservo.write(val);                  // sets the servo position according to the scaled value
  }
  return DONT_DRIVE;
} 

void loop()
{
  for (int i = 0; i < NUM_ROOMBAS; i++)
  {
    // Set which roomba the radio is sending to
    Radio_Set_Tx_Addr(ROOMBA_ADDRESSES[i]);
    
    int xVal = analogRead(xValPin[i]);            // reads the value of the potentiometer (value between 0 and 1023)
    int radius = joystick_x_mapping(xVal, ROOMBA_X_MAX);
    int yVal = analogRead(yValPin[i]);            // reads the value of the potentiometer (value between 0 and 1023)
    int velocity = joystick_y_mapping(yVal, ROOMBA_Y_MAX);
    
    if(velocity == DONT_DRIVE && radius == DONT_DRIVE)
    {
      // Send a STOP command
      if (acceleration > 0)
      {
        acceleration -= ACCELERATION_RATE;
      }
      else if (acceleration < 0)
      {
        acceleration += ACCELERATION_RATE;
      }
      
      
      if(acceleration == 0)
      {
        stopCommandsSent++;
        radius = 0;
      }
      packet.type = COMMAND;
      
      // Joystick pressed
      if (digitalRead(joyStickInPin[i]) == 1)
      {
        enableSend = true;
      }
      else if (enableSend && digitalRead(joyStickInPin[i]) == 0) // Joystick being p11111111111111111111ressed send a low value (0)
      {
        Serial.print("Sending byte\n");
        // sendByte = true; // Set the boolean value to 1 to be used in the interrupt
        enableSend = false;
        
        // Send encoded byte
        memcpy(ircommand_data.sender_address, ROOMBA_ADDRESSES[i], RADIO_ADDRESS_LENGTH);
        ircommand_data.ir_command = SEND_BYTE;
        ircommand_data.ir_data = VALUE_SENT;
        ircommand_data.servo_angle = 90;
        
        packet.type = IR_COMMAND;
        packet.payload.ir_command = ircommand_data;
        Serial.print("Byte sent\n");
      }
    }
    else
    {
      stopCommandsSent = 0;
      if(velocity == DONT_DRIVE)
      {
        acceleration = 200;
        if(radius > 0)
        {
          //counter-clockwise
          radius = -1;
        } 
        else if(radius < 0)
        {
          //clockwise
          radius = 1;
        }
      }
    }
    
    if (stopCommandsSent < STOP_COMMANDS_MAX || packet.type == IR_COMMAND)
    {
      if (packet.type == COMMAND)
      {
        if (velocity != DONT_DRIVE && abs(acceleration) < ROOMBA_Y_MAX)
        {
          if(velocity > 0)
          {
            acceleration += ACCELERATION_RATE;
          }
          else
          {
            acceleration -= ACCELERATION_RATE;
          }
        }
        memcpy(command_data.sender_address, ROOMBA_ADDRESSES[i], RADIO_ADDRESS_LENGTH);
        command_data.command = DRIVE_COMMAND;
        command_data.num_arg_bytes = DRIVE_NUM_BYTES;
        command_data.arguments[0] = (acceleration & HIGH_BYTE) >> 8;
        command_data.arguments[1] = acceleration & LOW_BYTE;
        command_data.arguments[2] = (radius & HIGH_BYTE) >> 8;
        command_data.arguments[3] = radius & LOW_BYTE;
        
        
        if(velocity == DONT_DRIVE && radius != DONT_DRIVE)
        {
          acceleration = 0;
        }
        
        if(radius == DONT_DRIVE)
        {
          command_data.arguments[2] = 0x80;
          command_data.arguments[3] = 0x00;
        }
        
        Serial.print("Radius: ");
        Serial.print(radius);
        Serial.print(" Velocity: ");
        Serial.print(velocity);
        Serial.print("\n");
        
        packet.type = COMMAND;
        packet.payload.command = command_data;
      }
      Radio_Transmit(&packet, RADIO_WAIT_FOR_TX);
    }
  }
  // delay(15); // waits for the servo to get there 
}
