/**
 * @file os.c
 *
 * @brief A Real Time Operating System
 *
 * Our implementation of the operating system described by Mantis Cheng in os.h.
 *
 * @author Scott Craig
 * @author Justin Tanner
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "os.h"
#include "kernel.h"
#include "error_code.h"
#include "ApplicationLayer.h"

/* Needed for memset */
/* #include <string.h> */

/** @brief main function provided by user application. The first task to run. */
extern int r_main();

/** The task descriptor of the currently RUNNING task. */
static task_descriptor_t* cur_task = NULL;

/** Since this is a "full-served" model, the kernel is executing using its own stack. */
static volatile uint16_t kernel_sp;

/** This table contains all task descriptors, regardless of state, plus idler. */
static task_descriptor_t task_desc[MAXPROCESS + 1];

/** The special "idle task" at the end of the descriptors array. */
static task_descriptor_t* idle_task = &task_desc[MAXPROCESS];

/** The current kernel request. */
static volatile kernel_request_t kernel_request;

/** Arguments for Task_Create() request. */
static volatile create_args_t kernel_request_create_args;

/** Return value for Task_Create() request. */
static volatile int kernel_request_retval;

/** Argument and return value for Service class of requests **/
static volatile SERVICE* kernel_request_service_ptr;
static volatile int16_t* kernel_request_service_v;

/** Number of tasks created so far */
static queue_t dead_pool_queue;

/** Number of services created so far */
static uint8_t num_services_created = 0;

/** The ready queue for RR tasks. Their scheduling is round-robin. */
static queue_t rr_queue;

/** The ready queue for SYSTEM tasks. Their scheduling is first come, first served. */
static queue_t system_queue;

/** The ready queue for PERIODIC tasks. Their scheduling is based on start, wcet, and period */
static queue_t periodic_queue;

/** An array of queues for tasks waiting on services. */
static queue_t service_queue[MAXSERVICE];

/** time remaining in current slot */
static volatile uint8_t ticks_remaining = 0;

/** this will be true if we are waiting for a periodic task to start */
static volatile bool waiting = false;

/** this will be true if Task_Terminate() got called for a periodic task */
static volatile bool terminating = false;

static uint16_t total_period_delta = 0;

/** Indicates if periodic task in this slot has already run this time */
static uint8_t slot_task_finished = 0;

/** Periodic task that should be executed */
static task_descriptor_t* periodic_task;

/** Error message used in OS_Abort() */
static uint8_t volatile error_msg = ERR_RUN_5_USER_CALLED_OS_ABORT;

static uint16_t TICKS_SINCE_INIT = 0;


/* Forward declarations */
/* kernel */
static void kernel_main_loop(void);
static void kernel_dispatch(void);
static void kernel_handle_request(void);
/* context switching */
static void exit_kernel(void) __attribute((noinline, naked));
static void enter_kernel(void) __attribute((noinline, naked));
extern "C" void TIMER1_COMPA_vect(void) __attribute__ ((signal, naked));

static int kernel_create_task();
static void kernel_terminate_task(void);
static void kernel_service_subscribe(volatile int16_t*);
static void kernel_service_publish(int16_t);
/* queues */

static void enqueue(queue_t* queue_ptr, task_descriptor_t* task_to_add);
static void enqueue_periodic(queue_t* queue_ptr, task_descriptor_t* task_to_add);
static task_descriptor_t* dequeue(queue_t* queue_ptr);
static task_descriptor_t* dequeue_periodic(queue_t* queue_ptr, task_descriptor_t* task_to_remove);

static void kernel_update_ticker(void);
static void idle (void);

/*
 * FUNCTIONS
 */
/**
 *  @brief The idle task does nothing but busy loop.
 */
static void idle (void)
{
    for(;;)
    {};
}


/**
 * @fn kernel_main_loop
 *
 * @brief The heart of the RTOS, the main loop where the kernel is entered and exited.
 *
 * The complete function is:
 *
 *  Loop
 *<ol><li>Select and dispatch a process to run</li>
 *<li>Exit the kernel (The loop is left and re-entered here.)</li>
 *<li>Handle the request from the process that was running.</li>
 *<li>End loop, go to 1.</li>
 *</ol>
 */
static void kernel_main_loop(void)
{
    for(;;)
    {
        kernel_dispatch();

        exit_kernel();

        /* if this task makes a system call, or is interrupted,
         * the thread of control will return to here. */

        kernel_handle_request();
    }
}


/**
 * @fn kernel_dispatch
 *
 *@brief The second part of the scheduler.
 *
 * Chooses the next task to run.
 *
 */
static void kernel_dispatch(void)
{
    /* If the current state is RUNNING, then select it to run again.
     * kernel_handle_request() has already determined it should be selected.
     */

    if(cur_task->state != RUNNING || cur_task == idle_task)
    {
		if(system_queue.head != NULL)
        {
			PORTG = DDRG;
			PORTC = 0;
            cur_task = dequeue(&system_queue);
        }
        else if(periodic_queue.head != NULL && !slot_task_finished && !waiting)
        {
            /* Keep running the current PERIODIC task. */
            cur_task = periodic_task;
			PORTC = DDRC;
			PORTG = 0;
        }
        else if(rr_queue.head != NULL && (periodic_queue.head == NULL || (periodic_queue.head != NULL && waiting)))
        {
            cur_task = dequeue(&rr_queue);
			PORTG = 0;
        }
        else
        {
            /* No task available, so idle. */
            cur_task = idle_task;
			PORTG = 0;
        }

        cur_task->state = RUNNING;
    }
}


/**
 * @fn kernel_handle_request
 *
 *@brief The first part of the scheduler.
 *
 * Perform some action based on the system call or timer tick.
 * Perhaps place the current process in a ready or waitng queue.
 */
static void kernel_handle_request(void)
{
   switch(kernel_request)
    {
    case NONE:
        /* Should not happen. */
        break;

    case TIMER_EXPIRED:
        kernel_update_ticker();
		
		TICKS_SINCE_INIT++;

        /* Round robin tasks get pre-empted on every tick. */
        if(cur_task->level == RR && cur_task->state == RUNNING)
        {
			PORTB = DDRB;
			PORTB = 0;
            cur_task->state = READY;
            enqueue(&rr_queue, cur_task);
        }
        break;

    case TASK_CREATE:
        kernel_request_retval = kernel_create_task();

        /* Check if new task has higer priority, and that it wasn't an ISR
         * making the request.
         */
        if(kernel_request_retval)
        {
            /* If new task is SYSTEM and cur is not, then don't run old one */
            if(kernel_request_create_args.level == SYSTEM && cur_task->level != SYSTEM)
            {
                cur_task->state = READY;
            }

            /* If cur is RR, it might be pre-empted by a new PERIODIC. */
            if(cur_task->level == RR &&
               kernel_request_create_args.level == PERIODIC)
            {
                cur_task->state = READY;
            }

            /* enqueue READY RR tasks. */
            if(cur_task->level == RR && cur_task->state == READY)
            {
                enqueue(&rr_queue, cur_task);
            }
        }
        break;

    case TASK_TERMINATE:
		if(cur_task != idle_task)
		{
        	kernel_terminate_task();
		}
        break;

    case TASK_NEXT:
		switch(cur_task->level)
		{
	    case SYSTEM:
	        enqueue(&system_queue, cur_task);
			break;

	    case PERIODIC:
	        slot_task_finished = 1;
	        break;

	    case RR:
	        enqueue(&rr_queue, cur_task);
	        break;

	    default: /* idle_task */
			break;
		}

		cur_task->state = READY;
        break;

    case TASK_GET_ARG:
        /* Should not happen. Handled in task itself. */
        break;
	
	case SERVICE_INIT:
		kernel_request_service_ptr = NULL;
		if(num_services_created < MAXSERVICE)
		{
			/* Pass a number back to the task, but pretend it is a pointer.
             * It is the index of the event_queue plus 1.
             * (0 is return value for failure.)
             */
            kernel_request_service_ptr = (SERVICE *)(uint16_t)(num_services_created + 1);

			++num_services_created;
		}
		else
		{
			kernel_request_service_ptr = (SERVICE *)(uint16_t)0;
		}
		break;

	case SERVICE_SUBSCRIBE:
		/* idle_task does not wait. */
		if(cur_task != idle_task)
		{
			kernel_service_subscribe(kernel_request_service_v);
		}
		break;

	case SERVICE_PUBLISH:
		kernel_service_publish(*kernel_request_service_v);
		break;
		
    default:
        /* Should never happen */
        error_msg = ERR_RUN_12_RTOS_INTERNAL_ERROR;
        OS_Abort();
        break;
    }

    kernel_request = NONE;
}


/*
 * Context switching
 */
/**
 * It is important to keep the order of context saving and restoring exactly
 * in reverse. Also, when a new task is created, it is important to
 * initialize its "initial" context in the same order as a saved context.
 *
 * Save r31 and SREG and EIND on stack, disable interrupts, then save
 * the rest of the registers on the stack. In the locations this macro
 * is used, the interrupts need to be disabled, or they already are disabled.
 */
#define    SAVE_CTX_TOP()       asm volatile (\
	"push   r31             \n\t"\
	"in     r31,0X3C        \n\t"\
    "push   r31             \n\t"\
    "in     r31,__SREG__    \n\t"\
    "cli                    \n\t"::); /* Disable interrupt */

#define STACK_SREG_SET_I_BIT()    asm volatile (\
    "ori    r31, 0x80        \n\t"::);

#define    SAVE_CTX_BOTTOM()       asm volatile (\
    "push   r31             \n\t"\
    "push   r30             \n\t"\
    "push   r29             \n\t"\
    "push   r28             \n\t"\
    "push   r27             \n\t"\
    "push   r26             \n\t"\
    "push   r25             \n\t"\
    "push   r24             \n\t"\
    "push   r23             \n\t"\
    "push   r22             \n\t"\
    "push   r21             \n\t"\
    "push   r20             \n\t"\
    "push   r19             \n\t"\
    "push   r18             \n\t"\
    "push   r17             \n\t"\
    "push   r16             \n\t"\
    "push   r15             \n\t"\
    "push   r14             \n\t"\
    "push   r13             \n\t"\
    "push   r12             \n\t"\
    "push   r11             \n\t"\
    "push   r10             \n\t"\
    "push   r9              \n\t"\
    "push   r8              \n\t"\
    "push   r7              \n\t"\
    "push   r6              \n\t"\
    "push   r5              \n\t"\
    "push   r4              \n\t"\
    "push   r3              \n\t"\
    "push   r2              \n\t"\
    "push   r1              \n\t"\
    "push   r0              \n\t"::);

/**
 * @brief Push all the registers and SREG and EIND onto the stack.
 */
#define    SAVE_CTX()    SAVE_CTX_TOP();SAVE_CTX_BOTTOM();

/**
 * @brief Pop all registers and the status register.
 */
#define    RESTORE_CTX_BOTTOM()    asm volatile (\
    "pop    r0                \n\t"\
    "pop    r1                \n\t"\
    "pop    r2                \n\t"\
    "pop    r3                \n\t"\
    "pop    r4                \n\t"\
    "pop    r5                \n\t"\
    "pop    r6                \n\t"\
    "pop    r7                \n\t"\
    "pop    r8                \n\t"\
    "pop    r9                \n\t"\
    "pop    r10             \n\t"\
    "pop    r11             \n\t"\
    "pop    r12             \n\t"\
    "pop    r13             \n\t"\
    "pop    r14             \n\t"\
    "pop    r15             \n\t"\
    "pop    r16             \n\t"\
    "pop    r17             \n\t"\
    "pop    r18             \n\t"\
    "pop    r19             \n\t"\
    "pop    r20             \n\t"\
    "pop    r21             \n\t"\
    "pop    r22             \n\t"\
    "pop    r23             \n\t"\
    "pop    r24             \n\t"\
    "pop    r25             \n\t"\
    "pop    r26             \n\t"\
    "pop    r27             \n\t"\
    "pop    r28             \n\t"\
    "pop    r29             \n\t"\
    "pop    r30             \n\t"::);

#define    RESTORE_CTX_TOP()    asm volatile (\
	"pop    r31             \n\t"\
	"out    __SREG__, r31   \n\t"\
	"pop    r31             \n\t"\
	"out    0X3C, r31    	\n\t"\
	"pop    r31             \n\t"::);
	
	/**
 * @brief Pop all registers and the status register.
 */
#define    RESTORE_CTX()    RESTORE_CTX_BOTTOM();RESTORE_CTX_TOP();


/**
 * @fn exit_kernel
 *
 * @brief The actual context switching code begins here.
 *
 * This function is called by the kernel. Upon entry, we are using
 * the kernel stack, on top of which is the address of the instruction
 * after the call to exit_kernel().
 *
 * Assumption: Our kernel is executed with interrupts already disabled.
 *
 * The "naked" attribute prevents the compiler from adding instructions
 * to save and restore register values. It also prevents an
 * automatic return instruction.
 */
static void exit_kernel(void)
{
    /*
     * The PC was pushed on the stack with the call to this function.
     * Now push on the I/O registers and the SREG and EIND as well.
     */
     SAVE_CTX();

    /*
     * The last piece of the context is the SP. Save it to a variable.
     */
    kernel_sp = SP;

    /*
     * Now restore the task's context, SP first.
     */
    SP = (uint16_t)(cur_task->sp);

    /*
     * Now restore I/O and SREG and EIND registers.
     */
    RESTORE_CTX();

    /*
     * return explicitly required as we are "naked".
     * Interrupts are enabled or disabled according to SREG
     * recovered from stack, so we don't want to explicitly
     * enable them here.
     *
     * The last piece of the context, the PC, is popped off the stack
     * with the ret instruction.
     */
    asm volatile ("ret\n"::);
}


/**
 * @fn enter_kernel
 *
 * @brief All system calls eventually enter here.
 *
 * Assumption: We are still executing on cur_task's stack.
 * The return address of the caller of enter_kernel() is on the
 * top of the stack.
 */
static void enter_kernel(void)
{
    /*
     * The PC was pushed on the stack with the call to this function.
     * Now push on the I/O registers and the SREG as well.
     */
    SAVE_CTX();

    /*
     * The last piece of the context is the SP. Save it to a variable.
     */
    cur_task->sp = (uint8_t*)SP;

    /*
     * Now restore the kernel's context, SP first.
     */
    SP = kernel_sp;

    /*
     * Now restore I/O and SREG registers.
     */
    RESTORE_CTX();

    /*
     * return explicitly required as we are "naked".
     *
     * The last piece of the context, the PC, is popped off the stack
     * with the ret instruction.
     */
    asm volatile ("ret\n"::);
}


/**
 * @fn TIMER1_COMPA_vect
 *
 * @brief The interrupt handler for output compare interrupts on Timer 1
 *
 * Used to enter the kernel when a tick expires.
 *
 * Assumption: We are still executing on the cur_task stack.
 * The return address inside the current task code is on the top of the stack.
 *
 * The "naked" attribute prevents the compiler from adding instructions
 * to save and restore register values. It also prevents an
 * automatic return instruction.
 */
void TIMER1_COMPA_vect(void)
{
	//PORTB ^= _BV(PB7);		// Arduino LED
    /*
     * Save the interrupted task's context on its stack,
     * and save the stack pointer.
     *
     * On the cur_task's stack, the registers and SREG are
     * saved in the right order, but we have to modify the stored value
     * of SREG. We know it should have interrupts enabled because this
     * ISR was able to execute, but it has interrupts disabled because
     * it was stored while this ISR was executing. So we set the bit (I = bit 7)
     * in the stored value.
     */
    SAVE_CTX_TOP();

    STACK_SREG_SET_I_BIT();

    SAVE_CTX_BOTTOM();

    cur_task->sp = (uint8_t*)SP;

    /*
     * Now that we already saved a copy of the stack pointer
     * for every context including the kernel, we can move to
     * the kernel stack and use it. We will restore it again later.
     */
    SP = kernel_sp;

    /*
     * Inform the kernel that this task was interrupted.
     */
    kernel_request = TIMER_EXPIRED;

    /*
     * Prepare for next tick interrupt.
     */
    OCR1A += TICK_CYCLES;

    /*
     * Restore the kernel context. (The stack pointer is restored again.)
     */
    SP = kernel_sp;

    /*
     * Now restore I/O and SREG registers.
     */
    RESTORE_CTX();

    /*
     * We use "ret" here, not "reti", because we do not want to
     * enable interrupts inside the kernel.
     * Explilictly required as we are "naked".
     *
     * The last piece of the context, the PC, is popped off the stack
     * with the ret instruction.
     */
    asm volatile ("ret\n"::);
}


/*
 * Tasks Functions
 */
/**
 *  @brief Kernel function to create a new task.
 *
 * When creating a new task, it is important to initialize its stack just like
 * it has called "enter_kernel()"; so that when we switch to it later, we
 * can just restore its execution context on its stack.
 * @sa enter_kernel
 */
static int kernel_create_task()
{
    /* The new task. */
    task_descriptor_t *p;
    uint8_t* stack_bottom;


    if (dead_pool_queue.head == NULL)
    {
        /* Too many tasks! */
        return 0;
    }

    if(kernel_request_create_args.level == PERIODIC &&
        (kernel_request_create_args.period == 0 ||
         kernel_request_create_args.wcet == 0))
    {
        /* PERIODIC period and wcet not specified */
        error_msg = ERR_2_CREATE_PERIODIC_INVALID_ARGS;
        OS_Abort();
    }

	/* idling "task" goes in last descriptor. */
	if(kernel_request_create_args.level == NULL)
	{
		p = &task_desc[MAXPROCESS];
	}
	/* Find an unused descriptor. */
	else
	{
	    p = dequeue(&dead_pool_queue);
	}

    stack_bottom = &(p->stack[WORKSPACE-1]);

    /* The stack grows down in memory, so the stack pointer is going to end up
     * pointing to the location 32 + 1 + 2 + 2 = 37 bytes above the bottom, to make
     * room for (from bottom to top):
     *   the address of Task_Terminate() to destroy the task if it ever returns,
     *   the address of the start of the task to "return" to the first time it runs,
     *   register 31,
     *   the stored SREG, and
	 *   the stored EIND, and
     *   registers 30 to 0.
     */
	int STACKCONTEXTSIZE = (32 + 1 + 1 + 3 + 3);
    uint8_t* stack_top = stack_bottom - STACKCONTEXTSIZE;
	
	for (int i = 0; i < 31; i++)
	{
		stack_top[i] = i;
	}
	stack_top[31] = 0x55;
	stack_top[32] = 0xEE;
	stack_top[33] = 31;
	

    /* Not necessary to clear the task descriptor. */
    /* memset(p,0,sizeof(task_descriptor_t)); */

    /* stack_top[0] is the byte above the stack.
     * stack_top[1] is r0. */
    stack_top[2] = (uint8_t) 0; /* r1 is the "zero" register. */
    /* stack_top[31] is r30. */
    stack_top[33] = (uint8_t) _BV(SREG_I); /* set SREG_I bit in stored SREG. */
	/* stack_top[STACKCONTEXTSIZE] is r31*/

    /* We are placing the address (16-bit) of the functions
     * onto the stack in reverse byte order (least significant first, followed
     * by most significant).  This is because the "return" assembly instructions
     * (ret and reti) pop addresses off in BIG ENDIAN (most sig. first, least sig.
     * second), even though the AT90 is LITTLE ENDIAN machine.
     */
	int KERNELARG_STACKOFFSET = (32 + 1 + 1 + 1);
	
    stack_top[KERNELARG_STACKOFFSET+0] = (uint8_t)(0);
    stack_top[KERNELARG_STACKOFFSET+1] = (uint8_t)((uint16_t)(kernel_request_create_args.f) >> 8);
    stack_top[KERNELARG_STACKOFFSET+2] = (uint8_t)(uint16_t)(kernel_request_create_args.f);
    stack_top[KERNELARG_STACKOFFSET+3] = (uint8_t)(0);
    stack_top[KERNELARG_STACKOFFSET+4] = (uint8_t)((uint16_t)Task_Terminate >> 8);
    stack_top[KERNELARG_STACKOFFSET+5] = (uint8_t)(uint16_t)Task_Terminate;

    /*
     * Make stack pointer point to cell above stack (the top).
     * Make room for 32 registers, SREG and two return addresses.
     */
    p->sp = stack_top;

    p->state = READY;
    p->arg = kernel_request_create_args.arg;
    p->level = kernel_request_create_args.level;

	switch(kernel_request_create_args.level)
	{
	case PERIODIC:
		/* Put this newly created PPP task into the PPP lookup array */
		p->start = kernel_request_create_args.start;
		p->wcet = kernel_request_create_args.wcet;
		p->period = kernel_request_create_args.period;
		enqueue_periodic(&periodic_queue, p);
		break;

    case SYSTEM:
    	/* Put SYSTEM and Round Robin tasks on a queue. */
        enqueue(&system_queue, p);
		break;

    case RR:
		/* Put SYSTEM and Round Robin tasks on a queue. */
        enqueue(&rr_queue, p);
		break;

	default:
		/* idle task does not go in a queue */
		break;
	}


    return 1;
}


/**
 * @brief Kernel function to destroy the current task.
 */
static void kernel_terminate_task(void)
{
    /* deallocate all resources used by this task */
    cur_task->state = DEAD;
    if(cur_task->level == PERIODIC)
    {
		// Periodic task termination and dead pool adding is handled differently. Handled in kernel_update_ticker
        terminating = true;
		slot_task_finished = 1;
    }
	else
	{
		enqueue(&dead_pool_queue, cur_task);
	}
}

/**
 * @brief Kernel function to place current task in a waiting queue.
 */
static void kernel_service_subscribe(volatile int16_t * v)
{
	// Toggle a pin to show a task was in here
	if (cur_task->level == RR)
	{
		PORTB = DDRB;
		PORTB = 0;
	}
	else if(cur_task->level == PERIODIC)
	{
		PORTC = DDRC;
		PORTC = 0;
	}
	else
	{
		PORTG = DDRG;
		PORTG = 0;
	}
    /* Check the handle of the event to ensure that it is initialized. */
    uint8_t handle = (uint8_t)((uint16_t)(kernel_request_service_ptr) - 1);

    if(handle >= num_services_created)
    {
        /* Error code. */
        error_msg = ERR_RUN_10_SUBSCRIBE_TO_BAD_SERVICE;
        OS_Abort();
    }
    else if(cur_task->level == PERIODIC)
	{
		error_msg = ERR_RUN_11_PERIODIC_CALLED_WAIT;
		OS_Abort();
	}
	else
    {
        /* Place this task in a queue. */
        cur_task->state = WAITING;
		cur_task->arg = v;
        enqueue(&service_queue[handle], cur_task);
    }
}

/**
 * @brief Kernel function to signal subscribed processes.
 *
 */
static void kernel_service_publish(int16_t v)
{
	// Toggle a pin to show a task was in here
	if (cur_task->level == RR)
	{
		PORTB = DDRB;
		PORTB = 0;
	}
	else if(cur_task->level == PERIODIC)
	{
		PORTC = DDRC;
		PORTC = 0;
	}
	else
	{
		PORTG = DDRG;
		PORTG = 0;
	}
	
    /* Check the handle of the event to ensure that it is initialized. */
    uint8_t handle = (uint8_t)((uint16_t)(kernel_request_service_ptr) - 1);

    if(handle >= num_services_created)
    {
        /* Error code. */
        error_msg = ERR_RUN_13_PUBLISH_ON_BAD_SERVICE;
        OS_Abort();
    }
    else
    {
        while(service_queue[handle].head != NULL)
        {
            /* The signaled task */
			task_descriptor_t* task_ptr = dequeue(&service_queue[handle]);
            task_ptr->state = READY;
			// Store the published value in the arg of the task
			*(task_ptr->arg) = v;

			// After the publish, put each task back in their respective queues
            switch(task_ptr->level)
            {
            case SYSTEM:
                enqueue(&system_queue, task_ptr);
                break;
            case PERIODIC:
				//Should never get here
                break;
            case RR:
                enqueue(&rr_queue, task_ptr);
                break;
            default:
                break;
            }
        }

        if(cur_task != idle_task)
        {
            cur_task->state = READY;
            if(cur_task->level == RR)
            {
                enqueue(&rr_queue, cur_task);
            }
        }
    }
}

/*
 * Queue manipulation.
 */

/**
 * @brief Add a task the head of the queue
 *
 * @param queue_ptr the queue to insert in
 * @param task_to_add the task descriptor to add
 */
static void enqueue(queue_t* queue_ptr, task_descriptor_t* task_to_add)
{
    task_to_add->next = NULL;

    if(queue_ptr->head == NULL)
    {
        /* empty queue */
        queue_ptr->head = task_to_add;
        queue_ptr->tail = task_to_add;
    }
    else
    {
        /* put task at the back of the queue */
        queue_ptr->tail->next = task_to_add;
        queue_ptr->tail = task_to_add;
    }
}

static void enqueue_periodic(queue_t* queue_ptr, task_descriptor_t* task_to_add)
{
	task_to_add->next = NULL;

	if(queue_ptr->head == NULL)
	{
		/* empty queue */
		queue_ptr->head = task_to_add;
		queue_ptr->tail = task_to_add;
		ticks_remaining = task_to_add->period;
		periodic_task = queue_ptr->head;
	}
	else
	{
		bool added = false;
		/* find the correct start time and period to add task */
		task_descriptor_t* comparing_task = queue_ptr->head;
		if (task_to_add->start < comparing_task->start && task_to_add->start + task_to_add->period <= comparing_task->start)
		{
			task_to_add->next = comparing_task;
			queue_ptr->head = task_to_add;
			added = true;
		}
		
		while (comparing_task != NULL && comparing_task->next != NULL && !added)
		{
			if (task_to_add->start < comparing_task->next->start && task_to_add->start + task_to_add->period <= comparing_task->next->start
				&& comparing_task->start + comparing_task->period <= task_to_add->start)
			{
				added = true;
				task_to_add->next = comparing_task->next;
				comparing_task->next = task_to_add;
			}
			else if (task_to_add->start > comparing_task->next->start)
			{
				comparing_task = comparing_task->next;
			}
			else
			{
				error_msg = ERR_4_PERIODIC_SCHEDULING_CONFLICT;
				OS_Abort();
			}
		}
		// If the task was not added, make this task the tail
		if (!added)
		{
			if (comparing_task->start + comparing_task->period <= task_to_add->start)
			{
				added = true;
				/* put task at the back of the queue */
				queue_ptr->tail->next = task_to_add;
				queue_ptr->tail = task_to_add;
			}
			else
			{
				error_msg = ERR_4_PERIODIC_SCHEDULING_CONFLICT;
				OS_Abort();
			}
		}
	}
}


/**
 * @brief Pops head of queue and returns it.
 *
 * @param queue_ptr the queue to pop
 * @return the popped task descriptor
 */
static task_descriptor_t* dequeue(queue_t* queue_ptr)
{
    task_descriptor_t* task_ptr = queue_ptr->head;

    if(queue_ptr->head != NULL)
    {
        queue_ptr->head = queue_ptr->head->next;
        task_ptr->next = NULL;
    }

    return task_ptr;
}

static task_descriptor_t* dequeue_periodic(queue_t* queue_ptr, task_descriptor_t* task_to_remove)
{
	task_descriptor_t* task_ptr = queue_ptr->head;
	task_descriptor_t* task_to_return = NULL;
	
	// Remove the head of the "queue"
	if (queue_ptr->head == task_to_remove)
	{
		queue_ptr->head = task_to_remove->next;
		enqueue(&dead_pool_queue, task_to_remove);
		task_to_return = queue_ptr->head;
	}
	else
	{
		while(task_ptr->next != NULL)
		{
			if (task_ptr->next->start == task_to_remove->start)
			{
				task_ptr->next = task_to_remove->next;
				task_to_return = task_ptr->next;
				task_to_remove->next = NULL;
				if (task_to_remove->start == queue_ptr->tail->start)
				{
					queue_ptr->tail = task_ptr;
					task_to_return = NULL;
				}
			}
			else
			{
				task_ptr = task_ptr->next;
			}
		}
	}
	return task_to_return;
}


/**
 * @brief Update the current time. Called every 5ms
 *
 * Perhaps move to the next time slot of the PPP.
 */
static void kernel_update_ticker(void)
{
    // If the head of the queue is NULL, we have no periodic tasks
    if(periodic_queue.head != NULL)
    {
        --ticks_remaining;

		// The waiting state is used when one periodic task finishes its period before the next one is supposed to start
		if(waiting)
		{
			// If we haven't reached Now(), don't start the task yet
			if(periodic_task->start > (TICKS_SINCE_INIT - total_period_delta))
			{
				return;
			}
			else
			{
				ticks_remaining = periodic_task->period;
				slot_task_finished = 0;
				waiting = false;
			}
		}
		else if(terminating)
		{
			// When terminating, remove the task from the periodic queue and go to the waiting state.
			PORTC = 0;
			// dequeue_periodic returns the next task to be ran
			periodic_task = dequeue_periodic(&periodic_queue, periodic_task);
			// If this is NULL, means that we reached the end of the queue, so reset to the head and set the delta to now
			if(periodic_task == NULL)
			{
				total_period_delta = TICKS_SINCE_INIT;
				periodic_task = periodic_queue.head;
			}
			terminating = false;
			waiting = true;
		}
        else if(ticks_remaining == 0)
        {
			PORTC = 0;
            /* If Periodic task still running then error */
            if(cur_task != NULL && cur_task->level == PERIODIC && slot_task_finished == 0)
            {
                /* error handling */
                error_msg = ERR_RUN_7_PERIODIC_TOOK_TOO_LONG;
                OS_Abort();
            }
			
			// Switch to next task
			periodic_task = periodic_task->next;
			
			if(periodic_task == NULL)
			{
				total_period_delta = TICKS_SINCE_INIT;
				periodic_task = periodic_queue.head;
			}
			// If we haven't reached Now(), don't start the task yet
			if(periodic_task->start > (TICKS_SINCE_INIT - total_period_delta))
			{
				waiting = true;
			}
			else
			{
				ticks_remaining = periodic_task->period;
				slot_task_finished = 0;
			}
        }
    }
	else
	{
		total_period_delta = TICKS_SINCE_INIT;
	}
}

#undef SLOW_CLOCK

#ifdef SLOW_CLOCK
/**
 * @brief For DEBUGGING to make the clock run slower
 *
 * Divide CLKI/O by 64 on timer 1 to run at 125 kHz  CS3[210] = 011
 * 1 MHz CS3[210] = 010
 */
static void kernel_slow_clock(void)
{
    TCCR1B &= ~(_BV(CS12) | _BV(CS10));
    TCCR1B |= (_BV(CS11));
}
#endif

/**
 * @brief Setup the RTOS and create main() as the first SYSTEM level task.
 *
 * Point of entry from the C runtime crt0.S.
 */
void OS_Init()
{
    int i;

    /* Set up the clocks */

    TCCR1B |= (_BV(CS11));

#ifdef SLOW_CLOCK
    kernel_slow_clock();
#endif

    /*
     * Initialize dead pool to contain all but last task descriptor.
     *
     * DEAD == 0, already set in .init4
     */
    for (i = 0; i < MAXPROCESS - 1; i++)
    {
        task_desc[i].state = DEAD;
        task_desc[i].next = &task_desc[i + 1];
    }
    task_desc[MAXPROCESS - 1].next = NULL;
    dead_pool_queue.head = &task_desc[0];
    dead_pool_queue.tail = &task_desc[MAXPROCESS - 1];

	/* Create idle "task" */
    kernel_request_create_args.f = (voidfuncvoid_ptr)idle;
    kernel_request_create_args.level = NULL;
    kernel_create_task();

    /* Create "main" task as SYSTEM level. */
    kernel_request_create_args.f = (voidfuncvoid_ptr)r_main;
    kernel_request_create_args.level = SYSTEM;
    kernel_create_task();

    /* First time through. Select "main" task to run first. */
    cur_task = task_desc;
    cur_task->state = RUNNING;
    dequeue(&system_queue);

    /* Set up Timer 1 Output Compare interrupt,the TICK clock. */
    TIMSK1 |= _BV(OCIE1A);
    OCR1A = TCNT1 + TICK_CYCLES;
    /* Clear flag. */
    TIFR1 = _BV(OCF1A);

    /*
     * The main loop of the RTOS kernel.
     */
    kernel_main_loop();
}




/**
 *  @brief Delay function adapted from <util/delay.h>
 */
void _delay_25ms(void)
{
	/*
    uint16_t i;

    // 4 * 50000 CPU cycles = 25 ms
    asm volatile ("1: sbiw %0,1" "\n\tbrne 1b" : "=w" (i) : "0" (5000));
	*/
	uint16_t start_time = Now();
	uint16_t current_time;
	for (;;)
	{
		current_time = Now();
		if(current_time - start_time >= 25)
		{
			break;
		}
	}
}


/** @brief Abort the execution of this RTOS due to an unrecoverable erorr.
 */
void OS_Abort(void)
{
    uint8_t i, j;
    uint8_t flashes, mask;

    Disable_Interrupt();

    /* Initialize port for output */
    DDRB = (uint8_t)(_BV(DDB7));
	PORTB = (uint8_t) 0;
	
	flashes = error_msg + 1;

    for(;;)
    {
        for(j = 0; j < flashes; ++j)
        {
            PORTB = DDRB;

            for(i = 0; i < 80; ++i)
            {
                _delay_ms(25);
            }

            PORTB = (uint8_t) 0;

            for(i = 0; i < 80; ++i)
            {
                _delay_ms(25);
            }
        }

        for(i = 0; i < 200; ++i)
        {
            _delay_ms(25);
        }
    }
}


/**
 * @param f  a parameterless function to be created as a process instance
 * @param arg an integer argument to be assigned to this process instanace
 * @param level assigned scheduling level: SYSTEM, PERIODIC or RR
 * @param name assigned PERIODIC process name
 * @return 0 if not successful; otherwise non-zero.
 * @sa Task_GetArg(), PPP[].
 *
 *  A new process  is created to execute the parameterless
 *  function @a f with an initial parameter @a arg, which is retrieved
 *  by a call to Task_GetArg().  If a new process cannot be
 *  created, 0 is returned; otherwise, it returns non-zero.
 *  The created process will belong to its scheduling @a level.
 *  If the process is PERIODIC, then its @a name is a user-specified name
 *  to be used in the PPP[] array. Otherwise, @a name is ignored.
 * @sa @ref policy
 */

 /**
   * \param f  a parameterless function to be created as a process instance
   * \param arg an integer argument to be assigned to this process instanace
   * \return 0 if not successful; otherwise non-zero.
   * \sa Task_GetArg()
   *
   *  A new process is created to execute the parameterless
   *  function \a f with an initial parameter \a arg, which is retrieved
   *  by a call to Task_GetArg().  If a new process cannot be
   *  created, 0 is returned; otherwise, it returns non-zero.
   *
   * \sa \ref policy
   */
int8_t   Task_Create_System(void (*f)(void), int16_t arg)
{
    int retval;
    uint8_t sreg;

    sreg = SREG;
    Disable_Interrupt();

    kernel_request_create_args.f = (voidfuncvoid_ptr)f;
    *(kernel_request_create_args.arg) = arg;
    kernel_request_create_args.level = SYSTEM;

    kernel_request = TASK_CREATE;
    enter_kernel();

    retval = kernel_request_retval;
    SREG = sreg;

    return retval;
}

int8_t   Task_Create_RR(    void (*f)(void), int16_t arg)
{
    int retval;
    uint8_t sreg;

    sreg = SREG;
    Disable_Interrupt();

    kernel_request_create_args.f = (voidfuncvoid_ptr)f;
    *(kernel_request_create_args.arg) = arg;
    kernel_request_create_args.level = RR;

    kernel_request = TASK_CREATE;
    enter_kernel();

    retval = kernel_request_retval;
    SREG = sreg;

    return retval;	
}

 /**
   * \param f a parameterless function to be created as a process instance
   * \param arg an integer argument to be assigned to this process instanace
   * \param period its execution period in TICKs
   * \param wcet its worst-case execution time in TICKs, must be less than "period"
   * \param start its start time in TICKs
   * \return 0 if not successful; otherwise non-zero.
   * \sa Task_GetArg()
   *
   *  A new process is created to execute the parameterless
   *  function \a f with an initial parameter \a arg, which is retrieved
   *  by a call to Task_GetArg().  If a new process cannot be
   *  created, 0 is returned; otherwise, it returns non-zero.
   *
   * \sa \ref policy
   */
int8_t   Task_Create_Periodic(void(*f)(void), int16_t arg, uint16_t period, uint16_t wcet, uint16_t start)
{
    int retval;
    uint8_t sreg;

    sreg = SREG;
    Disable_Interrupt();

    kernel_request_create_args.f = (voidfuncvoid_ptr)f;
    *(kernel_request_create_args.arg) = arg;
    kernel_request_create_args.level = PERIODIC;
	kernel_request_create_args.period = period;
	kernel_request_create_args.wcet = wcet;
	kernel_request_create_args.start = start;

    kernel_request = TASK_CREATE;
    enter_kernel();

    retval = kernel_request_retval;
    SREG = sreg;

    return retval;
}

/*
int Task_Create(void (*f)(void), int arg, unsigned int level, unsigned int name)
{
    int retval;
    uint8_t sreg;

    sreg = SREG;
    Disable_Interrupt();

    kernel_request_create_args.f = (voidfuncvoid_ptr)f;
    kernel_request_create_args.arg = arg;
    kernel_request_create_args.level = (uint8_t)level;
    kernel_request_create_args.name = (uint8_t)name;

    kernel_request = TASK_CREATE;
    enter_kernel();

    retval = kernel_request_retval;
    SREG = sreg;

    return retval;
}
*/

/**  
  * Returns the number of milliseconds since OS_Init(). Note that this number
  * wraps around after it overflows as an unsigned integer. The arithmetic
  * of 2's complement will take care of this wrap-around behaviour if you use
  * this number correctly.
  * Let  T = Now() and we want to know when Now() reaches T+1000.
  * Now() is always increasing. Even if Now() wraps around, (Now() - T) always
  * >= 0. As long as the duration of interest is less than the wrap-around time,
  * then (Now() - T >= 1000) would mean we have reached T+1000.
  * However, we cannot compare Now() against T directly due to this wrap-around
  * behaviour.
  * Now() will wrap around every 65536 milliseconds. Therefore, for measurement
  * purposes, it should be used for durations less than 65 seconds.
  */
uint16_t Now()
{
	// Handle overflow
	if (TICKS_SINCE_INIT > 65536/5)
	{
		TICKS_SINCE_INIT = 0;
	}
	// number of milliseconds since the RTOS boots.
	return TICKS_SINCE_INIT * 5;
}

/**
 * \return a non-NULL SERVICE descriptor if successful; NULL otherwise.
 *
 *  Initialize a new, non-NULL SERVICE descriptor.
 */
SERVICE *Service_Init()
{
	SERVICE* service_ptr;
	uint8_t sreg;
	
	sreg = SREG;
	Disable_Interrupt();
	
	kernel_request = SERVICE_INIT;
	enter_kernel();
	
	// Getting a 0 back means we have made too many services. Throw an error
	if((uint16_t)(kernel_request_service_ptr) == 0)
	{
		error_msg = ERR_RUN_14_TOO_MANY_SERVICES;
		OS_Abort();
	}
	service_ptr = (SERVICE *)kernel_request_service_ptr;

	SREG = sreg;

	return service_ptr;
}

/**  
  * \param s an Service descriptor
  * \param v pointer to memory where the received value will be written
  *
  * The calling task waits for the next published value associated with service "s".
  * More than one task may wait for a service. When a new value "v" is published to
  * "s", all waiting tasks resume and obtain the same value. 
  */
void Service_Subscribe( SERVICE *s, int16_t *v )
{
	uint8_t sreg;

	sreg = SREG;
	Disable_Interrupt();

	kernel_request = SERVICE_SUBSCRIBE;
	kernel_request_service_ptr = s;
	kernel_request_service_v = v;
	enter_kernel();

	SREG = sreg;
}

/**  
  * \param e a Service descriptor
  *
  * The calling task publishes a new value "v" to service "s". All waiting tasks on
  * service "s" will be resumed and receive a copy of this value "v". 
  * Values generated by services without subscribers will be lost.
  */
void Service_Publish( SERVICE *s, int16_t v )
{
	uint8_t sreg;

	sreg = SREG;
	Disable_Interrupt();

	kernel_request = SERVICE_PUBLISH;
	kernel_request_service_ptr = s;
	*kernel_request_service_v = v;
	enter_kernel();

	SREG = sreg;
}

/**
  * @brief The calling task gives up its share of the processor voluntarily.
  */
void Task_Next()
{
    uint8_t volatile sreg;

    sreg = SREG;
    Disable_Interrupt();

    kernel_request = TASK_NEXT;
    enter_kernel();

    SREG = sreg;
}


/**
  * @brief The calling task terminates itself.
  */
void Task_Terminate()
{
    uint8_t sreg;

    sreg = SREG;
    Disable_Interrupt();

    kernel_request = TASK_TERMINATE;
    enter_kernel();

    SREG = sreg;
}


/** @brief Retrieve the assigned parameter.
 */
volatile int16_t* Task_GetArg(void)
{
    volatile int16_t* arg;
    uint8_t sreg;

    sreg = SREG;
    Disable_Interrupt();

    arg = cur_task->arg;

    SREG = sreg;

    return arg;
}

/**
 * Runtime entry point into the program; just start the RTOS.  The application layer must define r_main() for its entry point.
 */
int main()
{
	// Enable LED for output
	// RR: Pin 13
	DDRB = (uint8_t)(_BV(DDB7));
	// System: Pin 41
	DDRG = (uint8_t)(_BV(DDG0));
	// Periodic: Pin 37
	DDRC = (uint8_t)(_BV(DDC0));
	// Profiling: Pin 5
	DDRE = (uint8_t)(_BV(DDE3));
	
	// Set both pins low
	PORTB = 0;
	PORTG = 0;
	PORTC = 0;
	PORTE = 0;
	
	/* Set up the clocks */
	CLOCK16MHZ();
	
	OS_Init();
	return 0;
}
